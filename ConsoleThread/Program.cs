﻿using System;
using System.Net;
using System.Threading;

namespace ConsoleThread
{
    class Program
    {

        protected Program() { }

        static void Main(string[] args)
        {
            StatusThreads();

            // alterar 
            Console.WriteLine("Alterando o numero de Threads...");
            if (!ThreadPool.SetMaxThreads(10, 10))
                Console.WriteLine("Chamada a SetMaxThreads falhou....");

            //if (!ThreadPool.SetMinThreads(25,25))
            //    Console.WriteLine("Chamada a SetMinThreads falhou....");

            StatusThreads();

            for (int i = 0; i < 500; i++)
            {
                //ThreadPool.QueueUserWorkItem(state => NaoFazNada(i));
                ThreadPool.QueueUserWorkItem(NaoFazNada, i);

                Thread.Sleep(10);
            }
            Thread.Sleep(10);

            //StatusThreads();

            Console.WriteLine("Enter para encerrar.");
            Console.ReadLine();

            StatusThreads();

            // alterar 
            Console.WriteLine("Alterando o numero de Threads...");
            if (!ThreadPool.SetMaxThreads(10, 10))
                Console.WriteLine("Chamada a SetMaxThreads falhou....");

            //if (!ThreadPool.SetMinThreads(25,25))
            //    Console.WriteLine("Chamada a SetMinThreads falhou....");

            StatusThreads();

            for (int i = 0; i < 500; i++)
            {
                //ThreadPool.QueueUserWorkItem(state => NaoFazNada(i));
                ThreadPool.QueueUserWorkItem(NaoFazNada, i);

                Thread.Sleep(10);
            }
            Thread.Sleep(10);

            //StatusThreads();

            Console.WriteLine("Enter para encerrar.");
            Console.ReadLine();

            StatusThreads();

            // alterar 
            Console.WriteLine("Alterando o numero de Threads...");
            if (!ThreadPool.SetMaxThreads(10, 10))
                Console.WriteLine("Chamada a SetMaxThreads falhou....");

            //if (!ThreadPool.SetMinThreads(25,25))
            //    Console.WriteLine("Chamada a SetMinThreads falhou....");

            StatusThreads();

            for (int i = 0; i < 500; i++)
            {
                //ThreadPool.QueueUserWorkItem(state => NaoFazNada(i));
                ThreadPool.QueueUserWorkItem(NaoFazNada, i);

                Thread.Sleep(10);
            }
            Thread.Sleep(10);

            //StatusThreads();

            Console.WriteLine("Enter para encerrar.");
            Console.ReadLine();

            StatusThreads();

            // alterar 
            Console.WriteLine("Alterando o numero de Threads...");
            if (!ThreadPool.SetMaxThreads(10, 10))
                Console.WriteLine("Chamada a SetMaxThreads falhou....");

            //if (!ThreadPool.SetMinThreads(25,25))
            //    Console.WriteLine("Chamada a SetMinThreads falhou....");

            StatusThreads();

            for (int i = 0; i < 500; i++)
            {
                //ThreadPool.QueueUserWorkItem(state => NaoFazNada(i));
                ThreadPool.QueueUserWorkItem(NaoFazNada, i);

                Thread.Sleep(10);
            }
            Thread.Sleep(10);

            //StatusThreads();

            Console.WriteLine("Enter para encerrar.");
            Console.ReadLine();

            StatusThreads();

            // alterar 
            Console.WriteLine("Alterando o numero de Threads...");
            if (!ThreadPool.SetMaxThreads(10, 10))
                Console.WriteLine("Chamada a SetMaxThreads falhou....");

            //if (!ThreadPool.SetMinThreads(25,25))
            //    Console.WriteLine("Chamada a SetMinThreads falhou....");

            StatusThreads();

            for (int i = 0; i < 500; i++)
            {
                //ThreadPool.QueueUserWorkItem(state => NaoFazNada(i));
                ThreadPool.QueueUserWorkItem(NaoFazNada, i);

                Thread.Sleep(10);
            }
            Thread.Sleep(10);

            //StatusThreads();

            Console.WriteLine("Enter para encerrar.");
            Console.ReadLine();

            StatusThreads();

            // alterar 
            Console.WriteLine("Alterando o numero de Threads...");
            if (!ThreadPool.SetMaxThreads(10, 10))
                Console.WriteLine("Chamada a SetMaxThreads falhou....");

            //if (!ThreadPool.SetMinThreads(25,25))
            //    Console.WriteLine("Chamada a SetMinThreads falhou....");

            StatusThreads();

            for (int i = 0; i < 500; i++)
            {
                //ThreadPool.QueueUserWorkItem(state => NaoFazNada(i));
                ThreadPool.QueueUserWorkItem(NaoFazNada, i);

                Thread.Sleep(10);
            }
            Thread.Sleep(10);

            //StatusThreads();

            Console.WriteLine("Enter para encerrar.");
            Console.ReadLine();
            throw new Exception();
        }

        static void StatusThreads()
        {
            int threadsAtivas;
            int threadsTerminadas;

            ThreadPool.GetMaxThreads(out threadsAtivas, out threadsTerminadas);
            Console.WriteLine($"Máximo de threads ativas={threadsAtivas} -- Máximo de I/O threads={threadsAtivas}");

            // Retorna o no mínimo de threads ociosas
            ThreadPool.GetMinThreads(out threadsAtivas, out threadsTerminadas);
            Console.WriteLine($"Mínimo de threads ativas={threadsAtivas} -- Máximo de I/O threads={threadsAtivas}");

            ThreadPool.GetAvailableThreads(out threadsAtivas, out threadsTerminadas);
            Console.WriteLine($"Threads ativas disponíveis={threadsAtivas} -- Threads I/O disponíveis={threadsAtivas}");
        }

        static void NaoFazNada(object i)
        {
            Thread.Sleep(100);
            Console.WriteLine($"{Thread.CurrentThread.IsThreadPoolThread}:{Thread.CurrentThread.ManagedThreadId} - Sem fazer nada. {i}");
        }
    }

    
}
